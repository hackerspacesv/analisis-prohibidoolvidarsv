# Herramientas de Extracción y Análisis del Hashtag ProhibidoOlvidarSV

Este repositorio contiene las herramientas de extracción y análisis para
el hashtag de Twitter #ProhibidoOlvidarSV.

El proyecto se realiza como una iniciativa ciudadana descentralizada para
recuperar la Memoria Colectiva del conflicto armado a través de compartir
historias del período de guerra utilizando el hashtag.

La mayoría de herramientas en este repositorio utilizan Python y los
procedimientos de análisis están documentados en Jupyter Notebooks.

Más información sobre esta iniciativa en:

[Análisis de Hashtag ProhibidoOlvidarSV](https://hackerspace.sv/wiki/Memoria_Historica_-_An%C3%A1lisis_de_Hashtag_ProhibidoOlvidarSV)

## Copyright
Debido a que esto es un esfuerzo colaborativo asegurate revisar las notas
de Copyright dentro de cada archivo ya que pueden variar según el colaborador.
