# Datasets
Este repositorio no incluye el dataset crudo en cumplimiento
a los términos de servicio de la API de Twitter.

Puedes usar los Scripts en las carpeta tools para descargar
la información.

Si eres un investigador o un individual interesado en realizar
análisis sobre la data, por favor escríbenos a info@hackerspace.sv
y con gusto te proporcionamos una base de datos de la extracción
tal y como fué extraída de Twitter.
