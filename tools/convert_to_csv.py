import json

f = open("../datasets/prohibidoOlvidarSV-Standard.json")

jsonAsString = f.read()

tweets = json.loads(jsonAsString)

print("id,date,is_retweet,full_text")
for tweet in tweets:
  print('{},"{}",{},"{}"'.format(
    tweet['id'],
    tweet['created_at'],
    '1' if tweet['full_text'].startswith('RT') else '0',
    tweet['full_text'].replace("\n","\\n").replace('"','""')
  ))

f.close()
