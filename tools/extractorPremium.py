import tweepy
import json
import os
import time
import random

import credentials

auth = tweepy.OAuthHandler(
    credentials.api_key,
    credentials.api_secret
  )

auth.set_access_token(
    credentials.token,
    credentials.secret
  )

api = tweepy.API(auth)

output_file = "../datasets/prohibidoOlvidarSV-PremiumAPI.json"
search_term = "#ProhibidoOlvidarSV"
environment = "testing"

cursor = tweepy.Cursor(
  # Cambiar a .search_full_archive luego del 11 de Marzo
  # de 2021
  api.search_30_day,
  environment_name=environment,
  query=search_term,
  fromDate="202101111400",
  toDate="202101171434",
  maxResults=100 # Maximo 500 con API Premium
)

f = open(output_file,"w+")

page_counter = 1
force_stop = False

latest_status = None
for page in cursor.pages():
  for status in page:
    f.write(",\n"+json.dumps(status._json))
    latest_status = status
    if break_on_id is not None and status.id <= break_on_id:
      force_stop = True
      print("---BEGINING OF OLD EXTRACTION---")
      break

  print("Last status ID {}".format(latest_status.id))
  print("Writting page {}".format(page_counter))
  page_counter+=1
  f.flush()
  # Sleep between 2 and 5 seconds to keep under rate limit
  random_sleep = 2+random.random()*5
  print("Sleeping for {}".format(random_sleep))
  time.sleep(random_sleep)
  if force_stop:
    break

f.close()
