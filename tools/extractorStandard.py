import tweepy
import json
import credentials
import os
import time
import random

auth = tweepy.OAuthHandler(
    credentials.api_key,
    credentials.api_secret
  )

auth.set_access_token(
    credentials.token,
    credentials.secret
  )

api = tweepy.API(auth, wait_on_rate_limit=True)

search_term = "#ProhibidoOlvidarSV"

output_file = "../datasets/prohibidoOlvidarSV-StandardAPI.json"

before_id = 1350555888327782400 # Tweet mas reciente
break_on_id = 1348638482588192769 # Tweet original

f = open(output_file,"w+")

cursor = tweepy.Cursor(
  api.search,
  q="#ProhibidoOlvidarSV",
  result_type="recent",
  tweet_mode="extended",
  max_id=before_id,
  count=100
)

page_counter = 0
force_stop = False

latest_status = None

for page in cursor.pages():
  for status in page:
    f.write(",\n"+json.dumps(status._json))
    latest_status = status
    if break_on_id is not None and status.id <= break_on_id:
      force_stop = True
      print("---BEGINING OF OLD EXTRACTION---")
      break

  print("Last status ID {}".format(latest_status.id))
  print("Writting page {}".format(page_counter))
  page_counter+=1
  f.flush()
  # Sleep between 2 and 5 seconds to keep under rate limit
  random_sleep = 2+random.random()*5
  print("Sleeping for {}".format(random_sleep))
  time.sleep(random_sleep)
  if force_stop:
    break

f.close()
